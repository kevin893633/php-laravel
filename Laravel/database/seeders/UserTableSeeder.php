<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->insert([
            'account' => str_random(5),
            'password' => Hash::make('password'),
            'name' => str_random(10),
            'email'    => str_random(10).'@mail.com',
            'modify_time'    => '2020-10-06 12:15:30',
            'ip' => '1.1.1.1',
        ]);
    }
}
